#!/usr/bin/python

# coded by xsecurity 
# madleets.com - sec4ever.com
# greets to : b0x - 1337 - sniffer 

import urllib2, sys

lst = open(sys.argv[1], 'r').read().split("\n")
for email in lst:
	twitter = urllib2.urlopen('https://twitter.com/users/email_available?email='+email).read()
	if 'Email has already been taken' in twitter:
		print '[+]'+email+' -> Registered !'
	else:
		print '[~]'+email+' -> Not Registerd'