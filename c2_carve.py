import re
import sys
import glob

"""
    Quick hack to carve out C2 from CVE-2017-0199 RTF files 
"""

def run(data):
    found_list =  re.findall(r'[a-fA-F0-9\x0D\x0A]{128,}',data)
    for item in found_list:
        try:
            temp = item.replace("\x0D\x0A","").decode("hex")
        except:
            # invalid data
            continue 
        # Find all unicode chars via http://stackoverflow.com/a/10645087
        pat = re.compile(ur'(?:[\x20-\x7E][\x00]){3,}')
        words = [w.decode('utf-16le') for w in pat.findall(temp)]
        for w in words:
            if "http" in w:
                print w 

for name in glob.glob('*'):
    run(open(name, "rb").read())
 
